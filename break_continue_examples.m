%% przykłady użycia poleceń break oraz continue
% na podstawie
% https://www.mathworks.com/help/matlab/ref/break.html
% https://www.mathworks.com/help/matlab/ref/continue.html

%% polecenie break przerywające pętle while
limit = 0.8;
s = 0;

while 1
    tmp = rand() % tmp = rand;  
    if tmp > limit
        break
    end
    s = s + tmp
end

%% polecenie continue wykorzystane w pętli for
for n = 1:50
    if mod(n,7)
        continue
    end
    disp(['Podzielne przez 7: ' num2str(n)])
end