%% porównanie kodu wykorzystującego pętle i kodu wektorowego
% na podstawie
% https://www.mathworks.com/help/matlab/matlab_prog/vectorization.html

%% wersja z pętlą
i = 0;
for t = 0:.01:10
    i = i + 1;
    y(i) = sin(t);
end

% inny sposób z pętlą 
t = 0:.01:10
for i = 1:length(t) 
    y(i) = sin(t(i));
end

% równoważny kod w wersji wektorowej
t = 0:.01:10;
y = sin(t);

%% obliczanie sum częściowych na co piątym elemencie
% wersja z pętlą
x = 1:10000;
tic
ylength = (length(x) - mod(length(x),5))/5;
y1(1:ylength) = 0;
for n= 5:5:length(x)
    y1(n/5) = sum(x(1:n));
end
toc

% wersja wektorowa
x = 1:10000;
tic
xsums = cumsum(x);
y2 = xsums(5:5:length(x)); 
toc

% sprawdzenie, że wyniki są tożsame
all(y1 == y2)

%% do wektoryzacji kodu przydatne są operatory macierzowe

% obliczenie objętości stożka
H = 1 % wysokość
D = 1 % średnica podstawy
V = 1/12*pi*(D^2)*H;

% pętla dla 10000 stożków
N = 10000
H = rand(1,N)
D = rand(1,N)

for n = 1:N
   V1(n) = 1/12*pi*(D(n)^2)*H(n);
end

% powyższa pętla zastąpiona zapisem wektorowym
% wykorzystującym operator macierzowy potęgowania .^
V2 = 1/12*pi*(D.^2).*H;

% sprawdzenie, że wyniki są tożsame
all(V1 == V2)

%% operatory macierzowe sprawdzają się dla danych w różnych wymiarach
s = 6;
H = zeros(s);

for c = 1:s
    for r = 1:s
        H(r,c) = 1/(r+c-1);
    end
end
disp(H)

c = [1:s]
r = [1:s]'
H(r,c) = 1./(r+c-1)
disp(H)

%% inny przykład
x = (-2:0.2:2)'; % 21 na 1 (kolumna)
y = -1.5:0.2:1.5; % 1 na 16 (wiersz)
F = x.*exp(-x.^2-y.^2); % 21 na 16 (macierz)

%% obliczanie silni z wykorzystaniem pętli
% oraz operacji wektorowych

n = 10;
f = n;
while n > 1
    n = n-1;
    f = f*n;
end
disp(['n! = ' num2str(f)])

n = 10
prod(1:n)    % iloczyn liczb od 1 do n
cumprod(1:n) % ciąg iloczynów częściowych liczb od 1 do n
