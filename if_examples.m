%% przykłady użycia instrukcji warunkowych
% na podstawie
% https://www.mathworks.com/help/matlab/ref/if.html
clear 
clc

%% proste porównanie
if 1 > 0 
    disp('1 > 0')
end

%% porównanie wektora liczb

limit = 0.75;
A = rand(5,1)

if any(A > limit)
    disp('Co najmniej jedna wartość przekracza granicę.')
else
    disp('Żadna wartość nie przekracza granicy.')
end

%% warunek złożony

x = 4;
minVal = 2;
maxVal = 6;

if (x >= minVal) && (x <= maxVal)
    disp('Wartość mieści się w zakresie.')
elseif (x > maxVal)
    disp('Wartość przekracza górną granicę.')
else
    disp('Wartość jest poniżej dolnej granicy.')
end