clear
clc

%% układ równań liniowych postaci
%  A*x = B 
A = magic(3)     
B = 15*ones(3,1)
x = A\B

%% układ równań liniowych postaci
%  A*x = B
%  dla macierzy nieosobliwej
A = magic(4)     
B = 34*ones(4,1)
x = A\B

%% układ równań liniowych postaci
%  A*x = B
%  dla macierzy nieosobliwej
A = [1 2; 1 2]
B = [1; 2]
x = A\B

%% niedookreślony układ równań
%  A*x = B
A = [1 2 0; 0 4 3];
b = [8; 18];
x = A\b

