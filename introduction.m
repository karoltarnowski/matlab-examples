clear
clc

%% tworzenie tablic (1)
x = [2 3]
y = [4;5]

%% tworzenie tablic (2)
x = [1 1 2; 3 5 8]

%% wektor równoodległych liczb
x = 3:2:9

%% funkcja linspace
x = linspace(0,5,11)
y = x'

%% tworzenie tablic
a = rand(5)
b = ones(3)
c = zeros(4)

%% odwoływanie się do elementów tablic
a34 = a(3,4)
a3x = a(3,:)
ax4 = a(:,4)
a(3,4) = 55

a5x = a(end,:)

%% funkcje działające na tablicach
x = rand(1,5)
maxx = max(x)
[maxx, indx] = max(x)

%% rysowanie wykresów
x = linspace(-2,2,5)
y = rand(size(x))

plot(x,y,'r--s')
% hold on
% plot(x,1-y,'k--s')

%% indeksowanie z wykorzystaniem
% wartości logicznych
x = rand(1,7)
x < 0.6
x(x<0.6)
x(x<0.6) = 0
y = x(x > 0.2 & x < 0.8)


