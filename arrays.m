clear
clc

%% macierze predefiniowane
A = ones(3,3); disp(A);
B = zeros(3); disp(B);
C = eye(3); disp(C);

%% dostęp blokowy do zmiennych
A = [1:4; 5:8; 9:12; 13:16]; disp(A);
A(2:3,2:3) = 0; disp(A);

%% usuwanie bloku - oprator []
A([3:4],:) = []; disp(A);

%% operacje na macierzach
A = [1:4; 5:8; 9:12; 13:16]; 

k = 2
a = triu(A,k)   %podmacierz trójkątna górna zaczynając od k-tej przekatnej 
a = tril(A,k)   %podmacierz trójkątna dolna zaczynając od k-tej przekatnej
a = diag(A,k)   %k-ta diagonalna z macierzy A utworzona jako kolumna
D = diag(a,k)   % macierz diagonalna z wektorem V na k-tej przekątnej
a = rot90(A, 1) % obrót macierzy A o k*90 stopni p. d. r. w. z.
a = fliplr(A)   % przerzucenie macierzy A w poziomie 
a = flipud(A)   % przerzucenie macierzy A w pionie 
m = 2                % liczba wierszy
n = 8                % liczba kolumn    
a = reshape(A, m, n) % zwinięcie wektora A (w pionie) w macierz o wymiarach [m, n]

%% transpozycja macierzy
A = magic(4)

% transpozycja
a = transpose(A)  % z wykorzystaniem funkcji
a = A.'           % z wykorzystaniem operatora .'

% transpozycja ze sprzężeniem
A = A + 1i
a = ctranspose(A) % z wykorzystaniem funkcji
a = A'            % z wykorzystaniem operatora '
