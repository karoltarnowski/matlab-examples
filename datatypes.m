clear
clc

%% przykłady zmiennych typu double
a = 1; % skalar (macierz 1 x 1)
b = [1]; 
c = [1, 2, 3]; % macierz wierszowa 1 x 3
d = [1; 2; 3]; % macierz kolumnowa 3 x 1
e = [1, 2, 3; 4, 5, 6; 7, 8, 9]; % „pełna" macierz 3 x 3
f = double(1);

%% przykłady łańcuchów znakowych i wektorów znakowych
g_str = "Hello, world!";
h_chr = 'Hello, world!';
i_chr = 'GCTAGAATCC';

%% przykład uchwytu do funkcji
j_hdl = @sin;
y = j_hdl(pi/2); % wywołanie równoważne y = sin(x)

%% przykłady zmiennych logicznych
k_logical = true;
l_logical = false;
m_logical = a < 5;

%%
whos
