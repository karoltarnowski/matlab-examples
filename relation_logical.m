% na podstawie
% https://www.mathworks.com/help/matlab/matlab_prog/find-array-elements-that-meet-a-condition.html
clear all

rng default      % reset generatora liczb losowych
A = randi(21,4)  % utworzenie tablicy przykładowych danych
B = randi(21,4)  % utworzenie tablicy przykładowych danych

C = A > 11       % utworzenie tablicy logicznej 
% z wykorzystaniem operatora relacji

D = A > B        % utworzenie tablicy logicznej 
% z wykorzystaniem operatora relacji

% indeksowanie z wykorzystaniem wartości logicznych
A(C)
B(D)

%%
% bardziej złożone wyrażenie logiczne
% wykorzystanie operatora logicznego
A(A<11 & A>2)

% to nie zadziała
A(2<A<11)

%%
% użycie innych operatorów logicznych
xor(C,D)
all(C)
any(C)

%%
% wymiary macierzy muszą być odpowiednie
y = randi(5,5,1)
x = [0:2:6]

y < x

%%
% wykorzystanie funkcji find do zamiany indeksowania
% logicznego na indeksowanie liniowe
find(D)
[r, c] = find(D)
[r, c, v] = find(D)

%%
% konwersja danych liczbowych na logiczne
logical(A)