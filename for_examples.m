%% przykłady użycia pętli for
% na podstawie
% www.mathworks.com/help/matlab/ref/for.html
clear
clc

%% zagnieżdżone pętle for po prostych wektorach
s = 6;
H = zeros(s);

for c = 1:s
    for r = 1:s
        H(r,c) = 1/(r+c-1);
    end
end

disp(H)

%% tworzenie macierzy z wykorzystaniem
% instrukcji warunkowej
ncols = 5
nrows = 6
for c = 1:ncols
    for r = 1:nrows
        if r == c
            A(r,c) = 2;
        elseif abs(r-c) == 1
            A(r,c) = -1;
        else
            A(r,c) = 0;
        end
    end
end
disp(A)

%% wektor elementów o niestandardowym kroku

for v = 1.0:-0.2:0.0
   disp(v)
end

%% wektor dowolnych elementów
values = rand(1,5)

for x = values
    disp(x)
end

%% tablica dowolnych elementów
values = rand(2,5)

for x = values
    disp('kolumna z macierzy values')
    disp(x)
end

