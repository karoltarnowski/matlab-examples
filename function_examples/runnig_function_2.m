%% wywołanie funkcji zdefiniowanych w skrypcie
z = 1:99;
ave = average2(z)
[m, s] = stat2(z)

%% przykład definicji funkcji w skrypcie
function ave = average2(x)
    ave = sum(x(:))/numel(x); 
end

function [m,s] = stat2(x)
    n = length(x);
    m = sum(x)/n;
    s = sqrt(sum((x-m).^2/n));
end