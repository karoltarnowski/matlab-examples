%% przykłady definicji funkcji anonimowych
% na podstawie
% https://www.mathworks.com/help/matlab/matlab_prog/anonymous-functions.html

%% prosta funkcja anonimowa

sqr = @(x) x.^2
sqr(3)

%% funkcja anonimowa korzystająca ze zmiennych

a = 2;
b = -3;
c = 15;
parabola = @(x) a*x.^2 + b*x + c;

clear a b c
x = 1;
y = parabola(x) % definicja funkcji anonimowej
% była utworzona, gdy zmienne a, b, c istniały

% przykład użycia funkcji anonimowej
% do sporzędzenia wykresu
fplot(parabola)

%% funkcja anonimowa o dwóch argumentach

myfunction = @(x,y) (x^2 + y^2 + x*y);

x = 1;
y = 10;
z = myfunction(x,y)
