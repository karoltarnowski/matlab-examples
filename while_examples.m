%% przykład użycia pętli while
% na podstawie
% https://www.mathworks.com/help/matlab/ref/while.html

%% obliczanie wartości silni z wykorzystaniem
% pętli while

n = 10;
f = n;
while n > 1
    n = n-1;
    f = f*n;
end
% disp(['n! = ' num2str(f)])
disp(['n! = ' num2str(f)])

%% istnieje również funkcja factorial
disp(['n! = ' num2str(factorial(10))])



